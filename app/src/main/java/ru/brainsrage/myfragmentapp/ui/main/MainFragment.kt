package ru.brainsrage.myfragmentapp.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.main_fragment.*
import kotlinx.android.synthetic.main.main_fragment.view.*
import ru.brainsrage.myfragmentapp.R

class MainFragment : Fragment() {

    companion object {
        fun newInstance() = MainFragment()
    }

    private lateinit var viewModel: MainViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val view = inflater.inflate(R.layout.main_fragment, container, false)
        view.search_button.setOnClickListener {
            viewModel.searchMovies(text_edit.text.toString())
            search_result.text = "Загрузка..."
            search_result.visibility = View.VISIBLE;
        }
        return view;
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        val recycleView = view?.findViewById<RecyclerView>(R.id.listView)
        recycleView?.layoutManager = LinearLayoutManager(requireContext())

        viewModel.movieListData.observe(viewLifecycleOwner, Observer {
            recycleView?.adapter = MovieAdapter(it)
            if (it.count() > 0) {
                search_result.visibility = View.INVISIBLE;
            } else {
                search_result.visibility = View.VISIBLE;
                search_result.text = "Нет результатов";
            }
        })
    }

}
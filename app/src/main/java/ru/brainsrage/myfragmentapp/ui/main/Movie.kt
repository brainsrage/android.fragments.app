package ru.brainsrage.myfragmentapp.ui.main

import com.google.gson.annotations.SerializedName

class Movie(
    @SerializedName("Title")
    val name: String,

    @SerializedName("imdbID")
    val imdbID: String,

    @SerializedName("Poster")
    val posterUrl: String
)
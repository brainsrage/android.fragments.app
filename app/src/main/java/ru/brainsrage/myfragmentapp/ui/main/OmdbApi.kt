package ru.brainsrage.myfragmentapp.ui.main

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface OmdbApi {
    @GET("/")
    suspend fun getAllMovies(
        @Query("s") search: String,
        @Query("r") responseType: String = "json",
        @Query("apiKey") apiKey: String = "85aa6049"
    ): Response<MovieResponse>

}
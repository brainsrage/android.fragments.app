package ru.brainsrage.myfragmentapp.ui.main

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class MainViewModel : ViewModel() {
    val movieListData: MutableLiveData<List<Movie>> = MutableLiveData();

    private val api: OmdbApi

    init {
        val retrofit = Retrofit.Builder()
            .baseUrl("http://www.omdbapi.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()
        api = retrofit.create(OmdbApi::class.java)
    }

    fun searchMovies(query: String) {
        viewModelScope.launch {
            val result = api.getAllMovies(query)
            result.body()?.movies?.forEach {
                Log.e("!@#", it.name)
            }
            if (result.body()?.movies !== null && result.body()?.movies!!.count() > 0) {
                result.body()?.movies?.let {
                    movieListData.value = it
                }
            } else {
                movieListData.value = listOf();
            }
        }
    }
}
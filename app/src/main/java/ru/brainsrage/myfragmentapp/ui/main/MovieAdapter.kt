package ru.brainsrage.myfragmentapp.ui.main

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ru.brainsrage.myfragmentapp.MainActivity
import ru.brainsrage.myfragmentapp.R

class MovieAdapter(val data: List<Movie>) : RecyclerView.Adapter<MovieAdapter.ViewHolder>() {
    inner class ViewHolder(val item: View) : RecyclerView.ViewHolder(item) {
        val nameTextView: TextView = itemView.findViewById(R.id.movie_name)
        val showButton: Button = itemView.findViewById(R.id.show_button)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        val contactView = inflater.inflate(R.layout.movie_list_item, parent, false)

        return ViewHolder(contactView)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val movie = data[position]
        holder.nameTextView.text = movie.name
        holder.showButton.setOnClickListener {
            val intent: Intent = Intent(holder.item.context, MainActivity::class.java)
            intent.putExtra("MOVIE_ID", movie.imdbID)
            holder.item.context.startActivity(intent)
            fm.beginTransaction().add(R.layout.container_id, OtherFragmentObject()).commit();
        }
        holder.showButton.setOnClickListener {
             View.OnClickListener()
        }
    }
}
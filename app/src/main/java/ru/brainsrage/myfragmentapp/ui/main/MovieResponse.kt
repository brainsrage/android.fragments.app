package ru.brainsrage.myfragmentapp.ui.main

import com.google.gson.annotations.SerializedName

class MovieResponse(
    @SerializedName("Search")
    val movies: List<Movie>
) {
}